
tmw-gmlog
==============================

Linux bash script to see the last n lines (default 10) of The Mana World GM logs. Designed to be used with a keyboard shortcut or similar method for quick reference.

Ledmitz (2020)
GPL-3.0
